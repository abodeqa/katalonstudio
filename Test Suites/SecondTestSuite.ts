<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SecondTestSuite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2017-11-19T00:15:41</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f6a198e3-9b08-44cf-9b72-61ca1daad7fe</testSuiteGuid>
   <testCaseLink>
      <guid>e60a991c-31b6-43e0-8387-02d8255c4d3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MultipleBrowserHandling/manualmode/SwitchingWindowUsingIndex</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1530c841-4690-4d82-84a3-7304230a61b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MultipleBrowserHandling/SwitchToWindowURL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6f39d68-a1f1-45e2-8b8e-8714ccba380c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MultipleBrowserHandling/WindowHandlingUsingIndex</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>788c71b8-2fcf-46b1-98d3-43ff226129be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MultipleBrowserHandling/WindowHandlingUsingTitle</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de6e86a9-dd34-4a62-ace2-784963a19c9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MouseActionExamples/DoubleClickAndFocusOnElementActionInManualMode</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e139ed5-1875-4aa9-9ad1-e6c400f6dfcf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MouseActionExamples/DoubleClickonElementInScriptMode</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9054f0be-c8bf-4de8-8c97-bca08724f3e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MouseActionExamples/DragAndDropActionInManualMode</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c7053ba-7cfa-400e-965f-a6a5b8b19268</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MouseActionExamples/HoverScrollFocusActionInManualMode</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48bb9a94-2fb3-4ba0-be43-00273a8467d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ManualModeScripting</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
