import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.internal.WebUIAbstractKeyword
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.support.ui.Select

WebUI.openBrowser("http://www.abodeqa.com/wp-content/uploads/2016/05/DemoSite.html")

WebUI.delay(3)

def driver = DriverFactory.getWebDriver()
def dropDownElement =  WebUIAbstractKeyword.findWebElement(findTestObject('Object Repository/abodeqaDemosite/Dropdwon'))

//Select Class in Selenium 
// When ever we need to interact with any of select /dropdown then we need one instance of Select class

def select = new Select(dropDownElement)

def list = select.getOptions()

list.each

WebUI.selectOptionByLabel(findTestObject('Object Repository/abodeqaDemosite/Dropdwon'), "Africa", false)

WebUI.delay(3)

WebUI.verifyOptionSelectedByLabel(findTestObject('Object Repository/abodeqaDemosite/Dropdwon'), "Africa", false, 0)

WebUI.closeBrowser()

