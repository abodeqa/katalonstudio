import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.googlecode.javacv.cpp.dc1394.pollfd
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//https://accounts.google.com/signin/oauth/identifier?client_id=917071888555.apps.googleusercontent.com&as=3231b6d70d837a5a&destination=https%3A%2F%2Fwww.quora.com&approval_state=!ChRsSEFoV0ZJVjBQZUF4TENab1NXehIfdzd0TnBmZ3ZSY1lXVU9xWmlfQjdtUl8wX3l6cC1oVQ%E2%88%99AHw7d_cAAAAAWgklKWUhXl7CWq3TZDblijMdAh17d5gw&passive=1209600&oauth=1&sarp=1&scc=1&xsrfsig=AHgIfE-4LFPE1IyHobTBF4Dd6DjqxGEVZQ&flowName=GeneralOAuthFlow


WebUI.openBrowser("https://www.quora.com/")

WebUI.maximizeWindow()

WebUI.click(findTestObject('Object Repository/Quora.com/policylink'))

WebUI.delay(4)

WebUI.switchToWindowUrl("https://www.quora.com/about/privacy")

WebUI.delay(2)
WebUI.setText(findTestObject('Object Repository/Quora.com/input_identifier'), "Demotestid@gmail.com")

WebUI.delay(2)

WebUI.closeWindowUrl("https://www.quora.com/about/privacy")

WebUI.closeBrowser()