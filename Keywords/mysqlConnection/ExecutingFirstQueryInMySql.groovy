package mysqlConnection

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import groovy.sql.Sql
import java.sql.*
public class ExecutingFirstQueryInMySql {

	
	@Keyword
	
	public connection(String url, String user, String password, String driverClassName){
		
		def sqlConnection = Sql.newInstance(url, user, password, driverClassName)
		
		sqlConnection.eachRow("Show tables") { row ->
			println row[0]
		}
		
		//Groovy Clouser - it is set of statement that is more like a method which return and 
	}
	
	@Keyword
	
	public createTable(String url, String user, String password, String driverClassName){
		
		def sqlConnection = Sql.newInstance(url, user, password, driverClassName)
		
		def createSQLQuery = """Create Table personDetail(
 firstname varchar(50), LastName varchar(90))"""

 sqlConnection.execute(createSQLQuery)
		
		sqlConnection.eachRow("Describe personDetail") { row ->
			println "Columnr " + row[0] + " Type of Column "+ row[1]
		}
		
		//Groovy Clouser - it is set of statement that is more like a method which return and
	}
	
	@Keyword
	
	public insertDataInToTable(String url, String user, String password, String driverClassName){
		
		def sqlConnection = Sql.newInstance(url, user, password, driverClassName)
		
		def createSQLQuery = """Create Table personDetail1(
 firstname varchar(50), LastName varchar(90))"""

 sqlConnection.execute(createSQLQuery)
 
 sqlConnection.execute("Insert into personDetail1 (firstname,LastName) values ('Jone', 'Ervin')")
 
 sqlConnection.eachRow("Select * from personDetail1") { row ->
	 
	 println "First Name = ${row[0]}, LanstName = ${row[1]}"
		
 
		
		//Groovy Clouser - it is set of statement that is more like a method which return and
	}
}
	
	@Keyword
	
	public deleterowAndTable(String url, String user, String password, String driverClassName){
		
		def sqlConnection = Sql.newInstance(url, user, password, driverClassName)
		
		sqlConnection.eachRow("Select count(*) from personDetail1") { row ->
			
			println "NumberRow before deleting the data = ${row[0]}"
		}
		//Groovy Clouser - it is set of statement that is more like a method which return and
			
			sqlConnection.execute("Delete from personDetail1 where LastName='Ervin' ")
			
			sqlConnection.eachRow("Select count(*) from personDetail1") { row ->
				
				println "NumberRow after deleting the data = ${row[0]}"
			}
			
		sqlConnection.execute("Drop Table personDetail")
		
		sqlConnection.eachRow("Show tables") { row ->
			println row[0]
		}
	}
}
